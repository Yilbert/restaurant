from django.contrib import admin
from django.urls import path
from rest_framework import routers
from restaurant_app import views


from restaurant_app.views import ClientListView,DetailsOrderView,CreateInvoice, List_Product,List_details_invoice, List_Waiter,ProductAPIView,InvoiceAPIView,edit_product,edit_client,edit_waiter,edit_table,CreateOrder,delete_client,delete_product,delete_table,delete_waiter
from restaurant_app.views import TableViewSet,WaiterViewSet, InvoiceViewSet ,DetailsViewSet, ClientViewSet,ProductViewSet



router = routers.DefaultRouter()
router.register(r'waiter',WaiterViewSet)
router.register(r'product',ProductViewSet)
router.register(r'invoice',InvoiceViewSet)
router.register(r'details',DetailsViewSet)
router.register(r'clients',ClientViewSet)
router.register(r'tables',TableViewSet)






urlpatterns = [
    path('list_waiters',ProductAPIView.as_view()),
    path('list_invoices/<int:id>',InvoiceAPIView.as_view()),

    path('',ClientListView.as_view(), name='list_client'),
    path('list_order',DetailsOrderView.as_view(),name='list_order'),
    path('list_product',List_Product.as_view(),name='list_product'),
    path('list_waiter',List_Waiter,name='list_waiter'),
    path('list_invoice/<int:id>',views.list_invoice, name='list_invoice'),
    path('list_details_invoice',List_details_invoice.as_view(), name='list_details_invoice'),

    path('create_product',views.CreateProduct, name='create_product'),
    path('create_client',views.CreateClient, name='create_client'),
    path('create_waiter',views.CreateWaiter, name='create_waiter'),
    path('create_table',views.CreateTable, name='create_table'),
    path('create_order',views.CreateOrder, name='create_order'),
    path('create_invoice',views.CreateInvoice, name='create_invoice'),

    path('edit_product/<int:id>', views.edit_product, name='edit_product'),
    path('edit_client/<int:id>', views.edit_client, name='edit_client'),
    path('edit_waiter/<int:id>', views.edit_waiter, name='edit_waiter'),
    path('edit_table/<int:id>', views.edit_table, name='edit_table'),

    path ('delete_client/<int:id>', views.delete_client,name='delete_client'),
    path ('delete_waiter/<int:id>', views.delete_waiter, name='delete_waiter'),
    path ('delete_table/<int:id>', views.delete_table, name='delete_table'),
    path ('delete_order/<int:id>', views.delete_order, name='delete_order'),
    path ('delete_product/<int:id>',views.delete_product, name='delete_product'),
    path ('delete_invoice/<int:id>', views.delete_invoice, name='delete_invoice'),
]
