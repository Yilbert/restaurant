from unicodedata import name
from django.db import models

# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    observation =models.TextField(blank=True)

    def __str__(self):
       return f'{self.name} {self.lastname}'


class Waiter(models.Model):
    name= models.CharField(max_length=100)
    lastname = models.CharField(max_length=100)
    lastname2= models.CharField(max_length=100)

    def __str__(self):
        return f'{self.name} {self.lastname}'


class Product(models.Model):
    name = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    price = models.FloatField()
    importe = models.IntegerField()

    def __str__(self):
        return f'{self.name} {self.price}'


class Table(models.Model):
    num_diners = models.IntegerField()
    ubications = models.CharField(max_length=100)

    def __str__(self):
        return f'Mesa {self.ubications}'


class Invoice(models.Model):
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    
    def __str__(self):
        return f'Factura Sr(a) {self.client_id.name}'



class DetailsOrder(models.Model):
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    invoice_id = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    waiter_id = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE)
    
    quantity = models.IntegerField()
    
    def total(self):
        resultado = self.product_id.price * self.quantity
        return resultado
