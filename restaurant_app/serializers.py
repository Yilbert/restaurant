from rest_framework import serializers

from restaurant_app.models import Product, Waiter, Invoice, DetailsOrder ,Client,Table


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [ 'name', 'type', 'price', 'importe']


class WaiterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waiter
        fields = ['id', 'name', 'lastname', 'lastname2']


class InvoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = '__all__'


class DetailOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = DetailsOrder
        fields = '__all__'

class ClientSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class TableSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Table
        fields = '__all__'        