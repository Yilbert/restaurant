from django.contrib import admin
from restaurant_app.models import Client,Product,Waiter,Table,DetailsOrder,Invoice
# Register your models here.


@admin.register(Client)
class Client(admin.ModelAdmin):
    list_display = ('id', 'name', 'lastname','observation')


@admin.register(Waiter)
class Waiter(admin.ModelAdmin):
    list_display = ('id', 'name', 'lastname','lastname2')


@admin.register(Product)
class Product(admin.ModelAdmin):
    list_display = ('id', 'name', 'type', 'price', 'importe')

@admin.register(Invoice)
class Invoice(admin.ModelAdmin):
    list_display = ('id', 'client_id')


@admin.register(Table)
class Table(admin.ModelAdmin):
    list_display = ('id', 'num_diners','ubications')


@admin.register(DetailsOrder)
class DetailsOrder(admin.ModelAdmin):
    list_display = ('id', 'product_id','invoice_id' ,'quantity', 'waiter_id', 'table_id')





