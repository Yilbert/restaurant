from django import forms
from .models import Product,Client,Waiter,Table,DetailsOrder,Invoice


class ProductForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    tipo = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    price = forms.FloatField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    importe = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))

    class Meta:
        model = Product
        fields = ['name', 'tipo',  'price','importe']



class ClientForm(forms.ModelForm):
    
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    lastname = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    observation= forms.CharField(widget=forms.Textarea(attrs={'class':'form-control mb-3'}))  

    class Meta:
        model = Client
        fields = ['name','lastname','observation']      



class WaiterForm(forms.ModelForm):   
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    lastname = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    lastname2= forms.CharField(widget=forms.TextInput(attrs={'class':'form-control mb-3'}))  

    class Meta:
        model = Waiter
        fields = ['name','lastname','lastname2']           



class TableForm(forms.ModelForm):   
    ubications = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    num_diners = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    class Meta:
        model = Table
        fields = ['ubications','num_diners'] 



class OrderForm(forms.ModelForm):   
  
    quantity = forms.IntegerField(widget=forms.TextInput(attrs={'class': 'form-control mb-3'}))
    class Meta:
        model = DetailsOrder
        fields = ['product_id', 'invoice_id', 'waiter_id', 'table_id', 'quantity']  


class InvoiceForm(forms.ModelForm):    
    class Meta:
        model = Invoice
        fields = ['client_id']                              