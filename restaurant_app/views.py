from django.shortcuts import render, redirect
from django.views.generic import ListView
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from restaurant_app.models import Client, DetailsOrder, Invoice, Waiter, Product, Table ,Client
from restaurant_app.serializers import TableSerializer, WaiterSerializer, ProductSerializer,InvoiceSerializer,DetailOrderSerializer,ClientSerializer, TableSerializer
from .forms import ProductForm, ClientForm, WaiterForm, TableForm, OrderForm,InvoiceForm


# Create your views here.

class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer
    # permission_classes = [permissions.IsAuthenticated]


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
   queryset = Invoice.objects.all()
   serializer_class = InvoiceSerializer


class DetailsViewSet(viewsets.ModelViewSet):
    queryset = DetailsOrder.objects.all()
    serializer_class = DetailOrderSerializer

class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer



class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer    

class ProductAPIView(APIView):

    def get(self, request):
        product = Product.objects.all()
        product_serializer = ProductSerializer(product, many=True)
        return Response(product_serializer.data)


class InvoiceAPIView(APIView):

    def get(self, request,id):
        details = DetailsOrder.objects.filter(invoice_id=id)
        details_serializer = DetailOrderSerializer(details, many=True)
        return Response(details_serializer.data)



class ClientListView(ListView):
    model = Client
    template_name = 'list_client.html'
    context_object_name = 'clients'


class DetailsOrderView(ListView):
    model = DetailsOrder
    template_name = 'list_order.html'
    context_object_name = 'orders'


def list_invoice(request, id):
    invoice = Invoice.objects.filter(id=id)
    client = Client.objects.get(id=id)
   
    order = DetailsOrder.objects.filter(invoice_id=id)
    
   
    if request.method == "GET":
        context = {
            'Invoice': invoice,
            'Client': client,
            'DetailsOrder': order,
            
          
        }
    return render(request, 'list_invoice.html', context)


class List_details_invoice(ListView):
    model = Invoice
    template_name = 'list_details_invoice.html'
    context_object_name = 'invoices'


class List_Product(ListView):
    model = Product
    template_name = 'list_product.html'
    context_object_name = 'products'


def List_Waiter(request):
    waiter = Waiter.objects.all()
    table = Table.objects.all()

    context = {
        'waiters': waiter,
        'tables': table
    }
    return render(request, 'list_waiter.html', context)


def CreateProduct(request):
    if request.method == "POST":
        form_prod = ProductForm(request.POST)
        if form_prod.is_valid():
            form_prod.save()
            return redirect('list_product')
    else:
        form_prod = ProductForm()
    return render(request, 'create_product.html', {'form_prod': form_prod})


def CreateClient(request):
    if request.method == "POST":
        form = ClientForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('create_table')
    else:
        form = ClientForm()
    return render(request, 'create_client.html', {'form': form})


def CreateWaiter(request):
    if request.method == "POST":
        form = WaiterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_waiter')
    else:
        form = WaiterForm()
    return render(request, 'create_waiter.html', {'form': form})


def CreateTable(request):
    if request.method == "POST":
        form = TableForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('create_invoice')
    else:
        form = TableForm()
    return render(request, 'create_table.html', {'form': form})


def CreateOrder(request):
    if request.method == "POST":
        form = OrderForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_details_invoice')
    else:
        form = OrderForm()
    return render(request, 'create_order.html', {'form': form})


def CreateInvoice(request):
    if request.method == "POST":
        form = InvoiceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('list_order')
    else:
        form = InvoiceForm()
    return render(request, 'create_invoice.html', {'form': form})



def edit_product(request, id):
    products = Product.objects.get(id=id)
    if request.method == "GET":
        form_prod = ProductForm(instance=products)
    else:
        form_prod = ProductForm(request.POST, instance=products)
        if form_prod.is_valid():
            form_prod.save()
            return redirect('list_product')
    return render(request, 'create_product.html', {'form_prod': form_prod})


def edit_client(request, id):
    clients = Client.objects.get(id=id)
    if request.method == "GET":
        form = ClientForm(instance=clients)
    else:
        form = ClientForm(request.POST, instance=clients)
        if form.is_valid():
            form.save()
            return redirect('list_client')
    return render(request, 'create_client.html', {'form': form})


def edit_waiter(request, id):
    waiters = Waiter.objects.get(id=id)
    if request.method == "GET":
        form = WaiterForm(instance=waiters)
    else:
        form = WaiterForm(request.POST, instance=waiters)
        if form.is_valid():
            form.save()
            return redirect('list_waiter')
    return render(request, 'create_waiter.html', {'form': form})

def edit_table(request, id):
    tables = Table.objects.get(id=id)
    if request.method == "GET":
        form = TableForm(instance=tables)
    else:
        form = TableForm(request.POST, instance=tables)
        if form.is_valid():
            form.save()
            return redirect('list_waiter')
    return render(request, 'create_table.html', {'form': form})     


def delete_client(request, id):
    client = Client.objects.get(id=id)
    client.delete()
    return redirect('list_client')


def delete_product(request, id):
    product = Product.objects.get(id=id)
    product.delete()
    return redirect('list_product')    


def delete_waiter(request, id):
    waiter = Waiter.objects.get(id=id)
    waiter.delete()
    return redirect('list_waiter')    


def delete_table(request, id):
    table = Table.objects.get(id=id)
    table.delete()
    return redirect('list_waiter')    


def delete_order(request, id):
    order = DetailsOrder.objects.get(id=id)
    order.delete()
    return redirect('list_order')   

def delete_invoice(request, id):
    invoice = Invoice.objects.get(id=id)
    invoice.delete()
    return redirect('list_details_invoice')     